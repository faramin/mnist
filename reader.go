package main

import (
	"fmt"
	"io"
	"os"

	"github.com/patrikeh/go-deep/training"
)

/*
	https://pjreddie.com/projects/mnist-in-csv/

	Tags:
	=====

	[offset] [type]          [value]          [description]
	0000     32 bit integer  0x00000801(2049) magic number (MSB first)
	0004     32 bit integer  60000            number of items
	0008     unsigned byte   ??               label
	0009     unsigned byte   ??               label
	........
	xxxx     unsigned byte   ??               label

	The labels values are 0 to 9.

	Images:
	=======

	[offset] [type]          [value]          [description]
	0000     32 bit integer  0x00000803(2051) magic number
	0004     32 bit integer  60000            number of images
	0008     32 bit integer  28               number of rows
	0012     32 bit integer  28               number of columns
	0016     unsigned byte   ??               pixel
	0017     unsigned byte   ??               pixel
	........
	xxxx     unsigned byte   ??               pixel
*/

func load(imgsPath, tagPath string) (training.Examples, error) {
	f, err := os.Open(imgsPath)
	defer f.Close()
	if err != nil {
		return nil, err
	}
	f2, err := os.Open(tagPath)
	defer f2.Close()
	if err != nil {
		return nil, err
	}

	_, count, row, col, err := readImgHeaders(f)
	if err != nil {
		return nil, err
	}

	_, count2, err := readTagHeaders(f2)
	if err != nil {
		return nil, err
	}
	if count2 != count {
		return nil, fmt.Errorf("invalid count")
	}

	examples := make(training.Examples, count)
	for i := 0; i < count; i++ {
		buffer := make([]byte, row*col)
		_, err := f.Read(buffer)
		if err != nil {
			return nil, err
		}
		examples[i].Input = bytes2input(buffer)

		buffer2 := make([]byte, 1)
		_, err = f2.Read(buffer2)
		if err != nil {
			return nil, err
		}
		if buffer2[0] > 9 {
			return nil, fmt.Errorf("invalid tag")
		}
		output := make([]float64, 10)
		output[buffer2[0]] = 1
		examples[i].Response = output
	}

	return examples, nil
}

func bytes2input(bytes []byte) []float64 {
	outp := make([]float64, len(bytes))
	for i, v := range bytes {
		outp[i] = float64(v) / 255.0
	}
	return outp
}

func read32bit(buf []byte) int {
	s := 0
	s += int(buf[0]) << 24
	s += int(buf[1]) << 16
	s += int(buf[2]) << 8
	s += int(buf[3])
	return s
}

func readTagHeaders(s io.Reader) (magic, count int, err error) {
	buffer := make([]byte, 4)

	_, err = s.Read(buffer)
	if err != nil {
		return
	}
	magic = read32bit(buffer)

	_, err = s.Read(buffer)
	if err != nil {
		return
	}
	count = read32bit(buffer)

	return
}

func readImgHeaders(s io.Reader) (magic, count, row, col int, err error) {
	buffer := make([]byte, 4)

	_, err = s.Read(buffer)
	if err != nil {
		return
	}
	magic = read32bit(buffer)

	_, err = s.Read(buffer)
	if err != nil {
		return
	}
	count = read32bit(buffer)

	_, err = s.Read(buffer)
	if err != nil {
		return
	}
	row = read32bit(buffer)

	_, err = s.Read(buffer)
	if err != nil {
		return
	}
	col = read32bit(buffer)

	return
}
