package main

import (
	"fmt"
	"math/rand"
	"time"

	deep "github.com/patrikeh/go-deep"
	"github.com/patrikeh/go-deep/training"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	train, err := load("./mnist_train_img.data", "./mnist_train_tag.data")
	if err != nil {
		panic(err)
	}
	train = train.SplitN(100)[0]
	fmt.Println(len(train))

	test, err := load("./mnist_test_img.data", "./mnist_test_tag.data")
	if err != nil {
		panic(err)
	}
	test = test.SplitN(100)[0]

	test.Shuffle()
	train.Shuffle()

	neural := deep.NewNeural(&deep.Config{
		Inputs:     len(train[0].Input),
		Layout:     []int{20, 20, 10},
		Activation: deep.ActivationSigmoid,
		Mode:       deep.ModeMultiClass,
		Weight:     deep.NewNormal(0.6, 0.1), // slight positive bias helps ReLU
		Bias:       true,
	})

	trainer := training.NewBatchTrainer(training.NewSGD(0.01, 0.5, 1e-6, true), 5, 200, 8)

	fmt.Printf("training: %d, test: %d\n", len(train), len(test))
	trainer.Train(neural, train, test, 500)
	fmt.Printf("Done\n")
}
